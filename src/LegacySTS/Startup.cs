﻿using Coscine.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Coscine.Api.LegacySTS
{
    public class Startup
    {
        private string _basePath;
        private IConfiguration _configuration { get; set; }

        internal IConfiguration GetConfiguration()
        {
            return _configuration;
        }

        public void SetBasePath(ApplicationInformation applicationInformation)
        {
            _basePath = $"/{applicationInformation.PathPrefix}";
            _configuration = new ConsulConfiguration();
        }

        public void ConfigureServicesExtension(IServiceCollection services)
        {

        }

        public void ConfigureServicesExtensionLate(IServiceCollection services)
        {

        }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureServicesExtension(services);

            services.AddMvc();

            ConfigureServicesExtensionLate(services);
        }

        public virtual void ConfigureExtension(IApplicationBuilder app, IHostingEnvironment env)
        {

        }

        public void ConfigureExtensionMiddleware(IApplicationBuilder app, IHostingEnvironment env)
        {

        }

        public void ConfigureExtensionLate(IApplicationBuilder app, IHostingEnvironment env)
        {

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfigureExtension(app, env);

            app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());

            ConfigureExtensionMiddleware(app, env);

            app.UsePathBase(_basePath);
            app.UseMvc();

            ConfigureExtensionLate(app, env);
        }
    }
}
