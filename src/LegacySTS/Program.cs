﻿using Coscine.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace Coscine.Api.LegacySTS
{
    class Program
    {
        static void Main(string[] args)
        {
            InitializeInternalWebService<Startup>();
        }

        public static IConfiguration Configuration = new ConsulConfiguration();


        public static void InitializeInternalWebService<ST>() where ST : Startup, new()
        {
            var configurator = new Configurator(new ApplicationInformation(), Configuration);
            configurator.Register();
            StartWebService<ST>(configurator);
        }

        private static void StartWebService<ST>(Configurator configurator) where ST : Startup, new()
        {
            ST startup = new ST();
            startup.SetBasePath(configurator.ApplicationInformation);

            var host = new WebHostBuilder()
            .ConfigureServices(services =>
            {
                services.AddSingleton(startup);
            })
            .UseStartup<ST>()
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseUrls($"http://[::]:{configurator.ApplicationInformation.Port}")
            .Build();

            host.Run();
        }
    }
}
