﻿using Coscine.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace Coscine.Api.LegacySTS
{
    public class Configurator
    {
        public ApplicationInformation ApplicationInformation { get; set; }
        public static IConfiguration Configuration { get; set; }

        public Configurator(ApplicationInformation applicationInformation, IConfiguration configuration)
        {
            ApplicationInformation = applicationInformation;
            Configuration = configuration;
            ApplicationInformation.ApiUrl = Configuration.GetString("coscine/local/api/additional/url");
        }
        public Configurator(IConfiguration configuration) : this(new ApplicationInformation(), configuration)
        {
        }

        public void SetApiUrl(string apiUrl)
        {
            ApplicationInformation.ApiUrl = apiUrl;
        }

        public void Register()
        {
            Register(ApplicationInformation);
        }

        public void Register(ApplicationInformation applicationInformation)
        {
            // Default app values
            var keys = Configuration.Keys(applicationInformation.AppBasePath);

            if (keys == null)
            {
                // No port overwrite
                if (applicationInformation.Port == 0)
                {
                    applicationInformation.Port = CalculatePort($"coscine/{applicationInformation.AppType}");
                }

                foreach (var kv in applicationInformation.AppValues.ToArray())
                {
                    Configuration.Put(kv.Key, kv.Value);
                }
            }
            else
            {
                // No port overwrite
                if (applicationInformation.Port == 0)
                {
                    int.TryParse(Configuration.GetString($"{applicationInformation.AppBasePath}/port"), out int port);
                    applicationInformation.Port = port;
                }
            }

            // Traefik Configuration
            foreach (var kv in applicationInformation.TraefikValues.ToArray())
            {
                Configuration.Put(kv.Key, kv.Value);
            }
        }

        private List<int> GetUsedConsulPorts(string appsPrefix)
        {
            var ports = new List<int>();
            var keys = Configuration.Keys(appsPrefix);

            if (keys != null)
            {
                foreach (var key in keys)
                {
                    // maybe find a better solution
                    if (key.EndsWith("/port") && Configuration.Get(key) != null)
                    {
                        if (int.TryParse(Configuration.GetString(key), out int p))
                        {
                            ports.Add(p);
                        }
                    }
                }
            }
            return ports;
        }

        private List<int> GetUsedSystemPorts()
        {
            return IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections().Select(x => x.LocalEndPoint.Port).ToList();
        }

        private int CalculatePort(string appsPrefix)
        {
            var usedPorts = GetUsedConsulPorts(appsPrefix).Union(GetUsedSystemPorts())
                .OrderBy(x => x);
            return Enumerable.Range(ApplicationInformation.PortRange.Item1, ApplicationInformation.PortRange.Item2 - ApplicationInformation.PortRange.Item1).Except(usedPorts).First();
        }
    }
}
