﻿using System;
using System.Security.Claims;

namespace Coscine.Api.LegacySTS.ModelingObjects
{
    [Serializable]
    public class SignInInstance
    {
        public ClaimsPrincipal User;
        public UserObject UserObject;

        public SignInInstance(ClaimsPrincipal user, UserObject userObject)
        {
            User = user;
            UserObject = userObject;
        }
    }
}
