﻿using System;

namespace Coscine.Api.LegacySTS.ModelingObjects
{
    [Serializable]
    public class UserObject
    {
        public string DisplayName;
        public Guid Id;
        public string EmailAddress;

        public UserObject(string displayName, Guid id, string emailAddress)
        {
            DisplayName = displayName;
            Id = id;
            EmailAddress = emailAddress;
        }
    }
}
