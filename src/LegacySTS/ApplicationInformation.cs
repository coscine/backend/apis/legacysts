﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Coscine.Api.LegacySTS
{
    public class ApplicationInformation
    {
        public string AppName { get; set; } = AssemblyName();
        public string AppType { get; set; } = "apis";
        public string ApiUrl { get; set; }
        public Version Version { get; set; } = ToSemanticVersion((System.Reflection.Assembly.GetEntryAssembly() != null) ? System.Reflection.Assembly.GetEntryAssembly().GetName().Version : System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
        public string DomainName { get; set; } = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        public string HostName { get; set; } = Dns.GetHostName();
        public string PathPrefix { get; set; } = $"coscine/api/{AssemblyName()}";
        public Tuple<int, int> PortRange = new Tuple<int, int>(6001, 6999);
        public int Port { get; set; } = 0;
        public string AppBasePath { get { return $"coscine/{AppType}/{AppName}"; } }
        public string TraefikBackendPath { get { return $"traefik/backends/{AppName}/servers/{HostName}"; } }
        public string TraefikFrontendPath { get { return $"traefik/frontends/{AppName}"; } }
        public Dictionary<string, string> AppValues
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { $"{AppBasePath}/port", $"{Port}" },
                    { $"{AppBasePath}/name", $"{AppName}" },
                    { $"{AppBasePath}/version", $"{Version}" }
                };
            }
        }
        public virtual Dictionary<string, string> TraefikValues
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { $"{TraefikBackendPath}/url", $"localhost:{Port}"},
                    { $"{TraefikBackendPath}/weight", $"{1}"},
                    { $"{TraefikFrontendPath}/backend", AppName},
                    { $"{TraefikFrontendPath}/routes/{AppName}/rule", $"Host:localhost;PathPrefix:/{PathPrefix}"}
                };
            }
            set { }
        }

        private static string AssemblyName()
        {
            return (System.Reflection.Assembly.GetEntryAssembly() != null) ? System.Reflection.Assembly.GetEntryAssembly().GetName().Name : System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }

        private static Version ToSemanticVersion(Version version)
        {
            return new Version(version.Major, version.Minor, version.Build);
        }
    }
}
