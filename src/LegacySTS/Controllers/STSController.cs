﻿using Coscine.Api.LegacySTS.ModelingObjects;
using Coscine.Api.LegacySTS.Security;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Configuration;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Web;

namespace Coscine.Api.LegacySTS.Controllers
{
    public class STSController : Controller
    {

        public STSController()
        {
        }

        [HttpPost("[controller]/processSignIn/{url}/{hostValue}")]
        public IActionResult ProcessSignIn(string url, string hostValue, [FromBody] SignInInstance signInInstance)
        {
            var user = signInInstance.User;
            var userObject = signInInstance.UserObject;

            var requestMessage = (SignInRequestMessage)WSFederationMessage.CreateFromUri(new Uri(HttpUtility.UrlDecode(url)));
            var signingCredentials = new X509SigningCredentials(CustomSecurityTokenService.GetCertificate());

            var config = new SecurityTokenServiceConfiguration(hostValue, signingCredentials);
            config.DefaultTokenLifetime = TimeSpan.FromDays(1);
            var sts = new CustomSecurityTokenService(config, userObject);
            SignInResponseMessage responseMessage = FederatedPassiveSecurityTokenServiceOperations.ProcessSignInRequest(requestMessage, user, sts);
            return Ok(responseMessage.WriteFormPost());
        }

    }
}
